FROM mhart/alpine-node:8.15.1
LABEL version="1.2.0"
LABEL maintainer="Michal Ochman <michal@kalambagames.com>"

WORKDIR /tmp

RUN \
  apk update &&\
  apk add ca-certificates &&\
  update-ca-certificates &&\
  apk --no-cache add \
    # - git access
    git \
    openssh-client \
    # - xargs with -P option
    findutils \
    # - https
    openssl \
    wget

RUN \
  # - phraseapp
  wget https://github.com/phrase/phraseapp-client/releases/latest/download/phraseapp_linux_386 -O /bin/phraseapp &&\
  chmod +x /bin/phraseapp &&\
  # - gettext
  npm install -g gettext.js@0.7.0

# cleanup
RUN \
  rm -rf /tmp/*

WORKDIR /
